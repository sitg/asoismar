<?php
/*
Plugin Name: Employment Exchange Maintenance
Plugin URI:
Description: This plugin implements a maintenance of offers and demands..
Version: 1.0
Author: Jeffry Miranda Toruño
Author URI:
License: GPL2
*/
/*  Copyright 2014  Matthew Van Andel  (email : matt@mattvanandel.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/* == NOTICE ===================================================================
 * Please do not alter this file. Instead: make a copy of the entire plugin, 
 * rename it, and work inside the copy. If you modify this plugin directly and 
 * an update is released, your changes will be lost!
 * ========================================================================== */

/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary. In this tutorial, we are
 * going to use the WP_List_Table class directly from WordPress core.
 *
 * IMPORTANT:
 * Please note that the WP_List_Table class technically isn't an official API,
 * and it could change at some point in the distant future. Should that happen,
 * I will update this plugin with the most current techniques for your reference
 * immediately.
 *
 * If you are really worried about future compatibility, you can make a copy of
 * the WP_List_Table class (file path is shown just below) to use and distribute
 * with your plugins. If you do that, just remember to change the name of the
 * class to avoid conflicts with core.
 *
 * Since I will be keeping this tutorial up-to-date for the foreseeable future,
 * I am going to work with the copy of the class provided in WordPress core.
 */
if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 *
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 *
 * Our theme for this list table is going to be companies.
 */
class EE_Company_List_Table extends WP_List_Table {

	/** ************************************************************************
	 * REQUIRED. Set up a constructor that references the parent constructor. We
	 * use the parent reference to set some default configs.
	 ***************************************************************************/
	function __construct(){
		global $status, $page;

		//Set parent defaults
		parent::__construct( array(
			'singular'  => 'company',     //singular name of the listed records
			'plural'    => 'companies',    //plural name of the listed records
			'ajax'      => false        //does this table support ajax?
		) );

	}

	/** ************************************************************************
	 * Recommended. This method is called when the parent class can't find a method
	 * specifically build for a given column. Generally, it's recommended to include
	 * one method for each column you want to render, keeping your package class
	 * neat and organized. For example, if the class needs to process a column
	 * named 'title', it would first see if a method named $this->column_title()
	 * exists - if it does, that method will be used. If it doesn't, this one will
	 * be used. Generally, you should try to use custom column methods as much as
	 * possible.
	 *
	 * Since we have defined a column_title() method later on, this method doesn't
	 * need to concern itself with any column with a name of 'title'. Instead, it
	 * needs to handle everything else.
	 *
	 * For more detailed insight into how columns are handled, take a look at
	 * WP_List_Table::single_row_columns()
	 *
	 * @param array $item A singular item (one full row's worth of data)
	 * @param array $column_name The name/slug of the column to be processed
	 * @return string Text or HTML to be placed inside the column <td>
	 **************************************************************************/
	function column_default($item, $column_name){
		switch($column_name){
			case 'company_place':
			case 'company_position':
			case 'company_vacancy':
			case 'company_contact':
			case 'company_phone':
			case 'company_email':
			case 'company_period':
			case 'company_comment':
				return $item[$column_name];
			default:
				return print_r($item,true); //Show the whole array for troubleshooting purposes
		}
	}

	/** ************************************************************************
	 * Recommended. This is a custom column method and is responsible for what
	 * is rendered in any column with a name/slug of 'title'. Every time the class
	 * needs to render a column, it first looks for a method named
	 * column_{$column_title} - if it exists, that method is run. If it doesn't
	 * exist, column_default() is called instead.
	 *
	 * This example also illustrates how to implement rollover actions. Actions
	 * should be an associative array formatted as 'slug'=>'link html' - and you
	 * will need to generate the URLs yourself. You could even ensure the links
	 *
	 * @see WP_List_Table::::single_row_columns()
	 * @param array $item A singular item (one full row's worth of data)
	 * @return string Text to be placed inside the column <td> (company title only)
	 **************************************************************************/
	function column_company_name($item){

		//Build row actions
		$actions = array(
			'edit'      => sprintf('<a href="?page=%s&action=%s&company=%s">Editar</a>','ee_company_form','edit',$item['company_id']),
			'delete'    => sprintf('<a id="delete" href="?page=%s&action=%s&company=%s">Eliminar</a>',$_REQUEST['page'],'delete',$item['company_id']),
		);

		//Return the title contents
		return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
			/*$1%s*/ $item['company_name'],
			/*$2%s*/ $item['company_id'],
			/*$3%s*/ $this->row_actions($actions)
		);
	}

	/** ************************************************************************
	 * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
	 * is given special treatment when columns are processed. It ALWAYS needs to
	 * have it's own method.
	 *
	 * @see WP_List_Table::::single_row_columns()
	 * @param array $item A singular item (one full row's worth of data)
	 * @return string Text to be placed inside the column <td> (company title only)
	 **************************************************************************/
	function column_cb($item){
		return sprintf(
			'<input type="checkbox" name="%1$s[]" value="%2$s" />',
			/*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("company")
			/*$2%s*/ $item['company_id']                //The value of the checkbox should be the record's id
		);
	}

	/** ************************************************************************
	 * REQUIRED! This method dictates the table's columns and titles. This should
	 * return an array where the key is the column slug (and class) and the value
	 * is the column's title text. If you need a checkbox for bulk actions, refer
	 * to the $columns array below.
	 *
	 * The 'cb' column is treated differently than the rest. If including a checkbox
	 * column in your table you must create a column_cb() method. If you don't need
	 * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
	 *
	 * @see WP_List_Table::::single_row_columns()
	 * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
	 **************************************************************************/
	function get_columns(){
		$columns = array(
			'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
			'company_name'     => 'Hotel',
			'company_place'    => 'Puesto',
			'company_position'  => 'Position',
			'company_vacancy'  => 'Puestos Vacantes',
			'company_contact'  => 'Contacto',
			'company_phone'  => 'Telefono',
			'company_email'  => 'Email',
			'company_period'  => 'Periodo',
			'company_comment'  => 'Comentarios'
		);
		return $columns;
	}

	/** ************************************************************************
	 * Optional. If you want one or more columns to be sortable (ASC/DESC toggle),
	 * you will need to register it here. This should return an array where the
	 * key is the column that needs to be sortable, and the value is db column to
	 * sort by. Often, the key and value will be the same, but this is not always
	 * the case (as the value is a column name from the database, not the list table).
	 *
	 * This method merely defines which columns should be sortable and makes them
	 * clickable - it does not handle the actual sorting. You still need to detect
	 * the ORDERBY and ORDER querystring variables within prepare_items() and sort
	 * your data accordingly (usually by modifying your query).
	 *
	 * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
	 **************************************************************************/
	function get_sortable_columns() {
		$sortable_columns = array(
			'company_name'     => array('company_name',false),     //true means it's already sorted
			'company_place'    => array('company_place',false),
			'company_position'  => array('company_position',false),
			'company_vacancy'  => array('company_vacancy',false),
			'company_contact'  => array('company_contact',false),
			'company_phone'  => array('company_phone',false),
			'company_email'  => array('company_email',false),
			'company_period'  => array('company_period',false),
			'company_comment'  => array('company_comment',false)
		);
		return $sortable_columns;
	}

	/** ************************************************************************
	 * Optional. If you need to include bulk actions in your list table, this is
	 * the place to define them. Bulk actions are an associative array in the format
	 * 'slug'=>'Visible Title'
	 *
	 * If this method returns an empty value, no bulk action will be rendered. If
	 * you specify any bulk actions, the bulk actions box will be rendered with
	 * the table automatically on display().
	 *
	 * Also note that list tables are not automatically wrapped in <form> elements,
	 * so you will need to create those manually in order for bulk actions to function.
	 *
	 * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
	 **************************************************************************/
	function get_bulk_actions() {
		$actions = array(
			'delete'    => 'Delete'
		);
		return $actions;
	}

	/** ************************************************************************
	 * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
	 * For this example package, we will handle it in the class to keep things
	 * clean and organized.
	 *
	 * @see $this->prepare_items()
	 **************************************************************************/
    function process_bulk_action($wpdb) {

        //Detect when a bulk action is being triggered...
        $targetDir =  ABSPATH . '/wp-admin/images/companies'.DIRECTORY_SEPARATOR;
        if( 'delete'===$this->current_action() ) {
            $deleting = isset( $_POST['deletecompany'] );
            if($deleting) {
                if ( is_array( $_REQUEST['company'] ) ) {
                    foreach ( $_REQUEST['company'] as $id ) {
                        $company_id= $id;
                        $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_companies WHERE company_id = %s",$company_id), ARRAY_A);
                        $company = $data[0];
                        $company_logo = $company['company_logo'];
                        $DelLogo = $targetDir.$company_logo;
                        if (file_exists($DelLogo)) {
                            unlink ($DelLogo);
                        }
                        $this->delete_company( $id );
                    }
                } else {
                    $company_id= $_REQUEST['company'];
                    $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_companies WHERE company_id = %s",$company_id), ARRAY_A);
                    $company = $data[0];
                    $company_logo = $company['company_logo'];
                    $DelLogo = $targetDir.$company_logo;
                    if (file_exists($DelLogo)) {
                        unlink ($DelLogo);
                    }
                    $this->delete_company( $company_id );
                }
            } else {?>
                <div class="wrap">
                <h2>Eliminar Hotel</h2><?php
                global $wpdb;
                $action = $_SERVER['REQUEST_URI'];?>
                <form method="post" action="<?php echo $action?>" name="deletecompany" id="deletecompany" class="validate" novalidate="novalidate">
                    <p>Estás a punto de eliminar el siguiente(s) hotel:</p>
                    <ul class="ul-disc"><?php
                        if ( is_array( $_REQUEST['company'] ) ) {
                            foreach ( $_REQUEST['company'] as $id ) {
                                $company_id= $id;
                                $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_companies WHERE company_id = %s",$company_id), ARRAY_A);
                                $company = $data[0];
                                $company_name = $company['company_name'];?>
                                <li><strong><?php echo $company_name?></strong></li><?php
                            }
                        } else {
                            $company_id= $_REQUEST['company'];
                            $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_companies WHERE company_id = %s",$company_id), ARRAY_A);
                            $company = $data[0];
                            $company_name = $company['company_name'];?>
                            <li><strong><?php echo $company_name?></strong></li><?php
                        }?>
                    </ul><?php
                    submit_button( __( 'Si, quiero borrar este hotel(s).' ), 'primary', 'deletecompany', true, array( 'id' => 'deletecompanysub' ) );?>
                </form>
                </div><?php
                wp_die();
            }
        }
    }

	public function delete_company($id)
	{
		global $wpdb;

		$deleted = $wpdb->delete( 'wp_companies', array ( 'company_id' => $id ) );

		return $deleted;
	}

	function process_edit_action($wpdb) {
		if( 'edit'===$this->current_action() ) {

            $server =  ABSPATH . '/wp-admin/images/companies'.DIRECTORY_SEPARATOR;

            $editing = isset( $_POST['editcompany'] );
            $md5_mail = md5($editing && isset( $_POST['company_email'] ) ? wp_unslash( $_POST['company_email'] ) : '');
            $extension = explode(".",$_FILES["company_logo"]["name"]);
            $logoName = $md5_mail. '.' .end($extension);

            if(empty($_FILES['company_logo']['name'])){
                $logoName = $_REQUEST['logo'];
            } else {
                $DelLogo = $server.$_REQUEST['logo'];
                if (file_exists($DelLogo)) {
                    unlink ($DelLogo);
                }
                //Writes the photo to the server
                if(move_uploaded_file($_FILES["company_logo"]['tmp_name'], $server.$logoName)){
                }
                else {
                    //Gives and error if its not
                    echo "Disculpe, hubo un error subiendo el logo al servidor. ".$server ;
                }
            }

			$data = array('company_name' => $editing && isset( $_POST['company_name'] ) ? wp_unslash( $_POST['company_name'] ) : '',
			              'company_place' => $editing && isset( $_POST['company_place'] ) ? wp_unslash( $_POST['company_place'] ) : '',
			              'company_position' => $editing && isset( $_POST['company_position'] ) ? wp_unslash( $_POST['company_position'] ) : '',
			              'company_vacancy' => $editing && isset( $_POST['company_vacancy'] ) ? wp_unslash( $_POST['company_vacancy'] ) : '',
			              'company_contact' => $editing && isset( $_POST['company_contact'] ) ? wp_unslash( $_POST['company_contact'] ) : '',
			              'company_phone' => $editing && isset( $_POST['company_phone'] ) ? wp_unslash( $_POST['company_phone'] ) : '',
			              'company_email' => $editing && isset( $_POST['company_email'] ) ? wp_unslash( $_POST['company_email'] ) : '',
			              'company_period' => $editing && isset( $_POST['company_period'] ) ? wp_unslash( $_POST['company_period'] ) : '',
			              'company_comment' => $editing && isset( $_POST['company_comment'] ) ? wp_unslash( $_POST['company_comment'] ) : '',
                          'company_logo' => $logoName
			);

			$wpdb->update( 'wp_companies', $data, array ('company_id' => $_REQUEST['company']));
			//wp_die('Coming soon!'.$_REQUEST['company']);
		}
	}

	function process_add_action($wpdb) {
		if( 'add'===$this->current_action() ) {

            $server =  ABSPATH . '/wp-admin/images/companies'.DIRECTORY_SEPARATOR;

            $creating = isset( $_POST['createcompany'] );
            $md5_mail =   md5($creating && isset( $_POST['company_email'] ) ? wp_unslash( $_POST['company_email'] ) : '');
            $temp = explode(".",$_FILES["company_logo"]["name"]);
            $company_logo = $md5_mail. '.' .end($temp);

			$data = array('company_name' => $creating && isset( $_POST['company_name'] ) ? wp_unslash( $_POST['company_name'] ) : '',
			              'company_place' => $creating && isset( $_POST['company_place'] ) ? wp_unslash( $_POST['company_place'] ) : '',
			              'company_position' => $creating && isset( $_POST['company_position'] ) ? wp_unslash( $_POST['company_position'] ) : '',
			              'company_vacancy' => $creating && isset( $_POST['company_vacancy'] ) ? wp_unslash( $_POST['company_vacancy'] ) : '',
			              'company_contact' => $creating && isset( $_POST['company_contact'] ) ? wp_unslash( $_POST['company_contact'] ) : '',
			              'company_phone' => $creating && isset( $_POST['company_phone'] ) ? wp_unslash( $_POST['company_phone'] ) : '',
			              'company_email' => $creating && isset( $_POST['company_email'] ) ? wp_unslash( $_POST['company_email'] ) : '',
			              'company_period' => $creating && isset( $_POST['company_period'] ) ? wp_unslash( $_POST['company_period'] ) : '',
			              'company_comment' => $creating && isset( $_POST['company_comment'] ) ? wp_unslash( $_POST['company_comment'] ) : '',
                          'company_logo' => $company_logo
					);

			$wpdb->insert('wp_companies', $data);

            //Writes the photo to the server
            if(move_uploaded_file($_FILES["company_logo"]['tmp_name'], $server.$company_logo)){
                //Tells you if its all ok
                //echo "The file ".basename( $_FILES['fileLogo']['name']). " has been uploaded, and your information has been added to the directory";
            }
            else {
                //Gives and error if its not
                echo "Disculpe, hubo un error subiendo el logo al servidor. ".$server;
            }
		}
	}

	/** ************************************************************************
	 * REQUIRED! This is where you prepare your data for display. This method will
	 * usually be used to query the database, sort and filter the data, and generally
	 * get it ready to be displayed. At a minimum, we should set $this->items and
	 * $this->set_pagination_args(), although the following properties and methods
	 * are frequently interacted with here...
	 *
	 * @global WPDB $wpdb
	 * @uses $this->_column_headers
	 * @uses $this->items
	 * @uses $this->get_columns()
	 * @uses $this->get_sortable_columns()
	 * @uses $this->get_pagenum()
	 * @uses $this->set_pagination_args()
	 **************************************************************************/
	function prepare_items() {
		global $wpdb; //This is used only if making any database queries

		/**
		 * First, lets decide how many records per page to show
		 */
		$per_page = 5;

		/**
		 * REQUIRED. Now we need to define our column headers. This includes a complete
		 * array of columns to be displayed (slugs & titles), a list of columns
		 * to keep hidden, and a list of columns that are sortable. Each of these
		 * can be defined in another method (as we've done here) before being
		 * used to build the value for our _column_headers property.
		 */
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();

		/**
		 * REQUIRED. Finally, we build an array to be used by the class for column
		 * headers. The $this->_column_headers property takes an array which contains
		 * 3 other arrays. One for all columns, one for hidden columns, and one
		 * for sortable columns.
		 */
		$this->_column_headers = array($columns, $hidden, $sortable);

		/**
		 * Optional. You can handle your bulk actions however you see fit. In this
		 * case, we'll handle them within our package just to keep things clean.
		 */
		$this->process_bulk_action($wpdb);

		$this->process_edit_action($wpdb);

		$this->process_add_action($wpdb);

		$data = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_companies", null), ARRAY_A);

		/**
		 * This checks for sorting input and sorts the data in our array accordingly.
		 *
		 * In a real-world situation involving a database, you would probably want
		 * to handle sorting by passing the 'orderby' and 'order' values directly
		 * to a custom query. The returned data will be pre-sorted, and this array
		 * sorting technique would be unnecessary.
		 */
		function usort_reorder($a,$b){
			$orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'company_name'; //If no sort, default to title
			$order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
			$result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
			return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
		}
		usort($data, 'usort_reorder');

		/**
		 * REQUIRED for pagination. Let's figure out what page the user is currently
		 * looking at. We'll need this later, so you should always include it in
		 * your own package classes.
		 */
		$current_page = $this->get_pagenum();

		/**
		 * REQUIRED for pagination. Let's check how many items are in our data array.
		 * In real-world use, this would be the total number of items in your database,
		 * without filtering. We'll need this later, so you should always include it
		 * in your own package classes.
		 */
		$total_items = count($data);

		/**
		 * The WP_List_Table class does not handle pagination for us, so we need
		 * to ensure that the data is trimmed to only the current page. We can use
		 * array_slice() to
		 */
		$data = array_slice($data,(($current_page-1)*$per_page),$per_page);

		/**
		 * REQUIRED. Now we can add our *sorted* data to the items property, where
		 * it can be used by the rest of the class.
		 */
		$this->items = $data;

		/**
		 * REQUIRED. We also have to register our pagination options & calculations.
		 */
		$this->set_pagination_args( array(
			'total_items' => $total_items,                  //WE have to calculate the total number of items
			'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
			'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
		) );
	}
}

class EE_Prospect_List_Table extends WP_List_Table {

    function __construct(){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'prospect',     //singular name of the listed records
            'plural'    => 'prospects',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
    }

    function column_default($item, $column_name){
        switch($column_name){
            case 'prospect_name':
            case 'prospect_phone':
            case 'prospect_email':
            case 'prospect_comment':
            case 'prospect_photo':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function column_prospect_name($item){

        //Build row actions
        $actions = array(
            'edit'      => sprintf('<a href="?page=%s&action=%s&prospect=%s">Editar</a>','ee_prospect_form','edit',$item['prospect_id']),
            'delete'    => sprintf('<a id="delete" href="?page=%s&action=%s&prospect=%s">Eliminar</a>',$_REQUEST['page'],'delete',$item['prospect_id']),
        );

        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
            /*$1%s*/ $item['prospect_name'],
            /*$2%s*/ $item['prospect_id'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("prospect")
            /*$2%s*/ $item['prospect_id']                //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){
        $columns = array(
            'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'prospect_name'     => 'Nombre',
            'prospect_phone'    => 'Telefono',
            'prospect_email'  => 'Email',
            'prospect_comment'  => 'Comentarios'
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'prospect_name'     => array('prospect_name',false),     //true means it's already sorted
            'prospect_phone'    => array('prospect_phone',false),
            'prospect_email'  => array('prospect_email',false),
            'prospect_comment'  => array('prospect_comment',false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action($wpdb) {

        //Detect when a bulk action is being triggered...
        $targetDir =  ABSPATH . '/wp-admin/images/prospects'.DIRECTORY_SEPARATOR;
        if( 'delete'===$this->current_action() ) {
            $deleting = isset( $_POST['deleteprospect'] );
            if($deleting) {
                if ( is_array( $_REQUEST['prospect'] ) ) {
                    foreach ( $_REQUEST['prospect'] as $id ) {
                        $prospect_id= $id;
                        $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_prospects WHERE prospect_id = %s",$prospect_id), ARRAY_A);
                        $prospect = $data[0];
                        $prospect_photo = $prospect['prospect_photo'];
                        $DelPhoto = $targetDir.$prospect_photo;
                        if (file_exists($DelPhoto)) {
                            unlink ($DelPhoto);
                        }
                        $this->delete_prospect( $id );
                    }
                } else {
                    $prospect_id= $_REQUEST['prospect'];
                    $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_prospects WHERE prospect_id = %s",$prospect_id), ARRAY_A);
                    $prospect = $data[0];
                    $prospect_photo = $prospect['prospect_photo'];
                    $DelPhoto = $targetDir.$prospect_photo;
                    if (file_exists($DelPhoto)) {
                        unlink ($DelPhoto);
                    }
                    $this->delete_prospect( $prospect_id );
                }
            } else {?>
                <div class="wrap">
                <h2>Eliminar Prospecto</h2><?php
                global $wpdb;
                $action = $_SERVER['REQUEST_URI'];?>
                <form method="post" action="<?php echo $action?>" name="deleteprospect" id="deleteprospect" class="validate" novalidate="novalidate">
                    <p>Estás a punto de eliminar el siguiente prospecto(s):</p>
                    <ul class="ul-disc"><?php
                        if ( is_array( $_REQUEST['prospect'] ) ) {
                            foreach ( $_REQUEST['prospect'] as $id ) {
                                $prospect_id= $id;
                                $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_prospects WHERE prospect_id = %s",$prospect_id), ARRAY_A);
                                $prospect = $data[0];
                                $prospect_name = $prospect['prospect_name'];?>
                                <li><strong><?php echo $prospect_name?></strong></li><?php
                            }
                        } else {
                            $prospect_id= $_REQUEST['prospect'];
                            $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_prospects WHERE prospect_id = %s",$prospect_id), ARRAY_A);
                            $prospect = $data[0];
                            $prospect_name = $prospect['prospect_name'];?>
                            <li><strong><?php echo $prospect_name?></strong></li><?php
                        }?>
                    </ul><?php
                    submit_button( __( 'Si, quiero borrar este prospecto.' ), 'primary', 'deleteprospect', true, array( 'id' => 'deleteprospectsub' ) );?>
                </form>
                </div><?php
                wp_die();
            }
        }
    }

    public function delete_prospect($id)
    {
        global $wpdb;

        $deleted = $wpdb->delete( 'wp_prospects', array ( 'prospect_id' => $id ) );

        return $deleted;
    }

    function process_edit_action($wpdb) {
        if( 'edit'===$this->current_action() ) {

            $server =  ABSPATH . '/wp-admin/images/prospects'.DIRECTORY_SEPARATOR;

            $editing = isset( $_POST['editprospect'] );
            $md5_mail =   md5($editing && isset( $_POST['prospect_email'] ) ? wp_unslash( $_POST['prospect_email'] ) : '');
            $temp = explode(".",$_FILES["prospect_photo"]["name"]);
            $photo = $md5_mail. '.' .end($temp);

            if(empty($_FILES['prospect_photo']['name'])){
                $photo = $_REQUEST['photo'];
            } else {
                $DelPhoto = $server.$_REQUEST['photo'];
                if (file_exists($DelPhoto)) {
                    unlink ($DelPhoto);
                }
                //Writes the photo to the server
                if(move_uploaded_file($_FILES["prospect_photo"]['tmp_name'], $server.$photo)){
                }
                else {
                    //Gives and error if its not
                    echo "Disculpe, hubo un error subiendo su foto al servidor. ".$server;
                }
            }
            $data = array('prospect_name' => $editing && isset( $_POST['prospect_name'] ) ? wp_unslash( $_POST['prospect_name'] ) : '',
                'prospect_phone' => $editing && isset( $_POST['prospect_phone'] ) ? wp_unslash( $_POST['prospect_phone'] ) : '',
                'prospect_email' => $editing && isset( $_POST['prospect_email'] ) ? wp_unslash( $_POST['prospect_email'] ) : '',
                'prospect_comment' => $editing && isset( $_POST['prospect_comment'] ) ? wp_unslash( $_POST['prospect_comment'] ) : '',
                'prospect_photo' => $photo
            );

            $wpdb->update( 'wp_prospects', $data, array ('prospect_id' => $_REQUEST['prospect']));
        }
    }

    function process_add_action($wpdb) {
        if( 'add'===$this->current_action() ) {

            $server = ABSPATH . 'wp-admin/images/prospects'.DIRECTORY_SEPARATOR;

            $creating = isset( $_POST['createprospect'] );
            $md5_mail =   md5($creating && isset( $_POST['prospect_email'] ) ? wp_unslash( $_POST['prospect_email'] ) : '');
            $temp = explode(".",$_FILES["prospect_photo"]["name"]);
            $prospect_photo = $md5_mail. '.' .end($temp);
            $data = array(
                'prospect_name' => $creating && isset( $_POST['prospect_name'] ) ? wp_unslash( $_POST['prospect_name'] ) : '',
                'prospect_phone' => $creating && isset( $_POST['prospect_phone'] ) ? wp_unslash( $_POST['prospect_phone'] ) : '',
                'prospect_email' => $creating && isset( $_POST['prospect_email'] ) ? wp_unslash( $_POST['prospect_email'] ) : '',
                'prospect_comment' => $creating && isset( $_POST['prospect_comment'] ) ? wp_unslash( $_POST['prospect_comment'] ) : '',
                'prospect_photo' => $prospect_photo
            );

            $wpdb->insert('wp_prospects', $data);

            //Writes the photo to the server
            if(move_uploaded_file($_FILES["prospect_photo"]['tmp_name'], $server.$prospect_photo)){
                //rename($target,$prospect_name.'jpg');
                //Tells you if its all ok
                //echo "The file ".basename( $_FILES['fileLogo']['name']). " has been uploaded, and your information has been added to the directory";
            }
            else {
                //Gives and error if its not
                echo "Disculpe, hubo un error subiendo su foto al servidor. ".$server;
            }
        }
    }

    function prepare_items() {
        global $wpdb; //This is used only if making any database queries

        $per_page = 5;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action($wpdb);

        $this->process_edit_action($wpdb);

        $this->process_add_action($wpdb);

        $data = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_prospects", null), ARRAY_A);

        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'prospect_name'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);

        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
}

/** ************************ REGISTER THE TEST PAGE ****************************
 *******************************************************************************
 * Now we just need to define an admin page. For this example, we'll add a top-level
 * menu item to the bottom of the admin menus.
 */
function ee_add_menu_items(){
	add_menu_page('Acerca', 'Bolsa de Empleo', 'manage_options', 'ee_about', 'ee_render_about_page');
    add_submenu_page( 'ee_about', 'Ofertas', 'Todas las Ofertas', 'manage_options', 'ee_prospects', 'ee_render_prospects_page');
    add_submenu_page( 'ee_about', 'Ofertas', 'Añadir Ofertas', 'manage_options', 'ee_prospect_form', 'ee_render_prospect_form' );
	add_submenu_page( 'ee_about', 'Demandas', 'Todas las Demandas', 'manage_options', 'ee_companies', 'ee_render_companies_page');
	add_submenu_page( 'ee_about', 'Demandas', 'Añadir Demanda', 'manage_options', 'ee_company_form', 'ee_render_company_form' );
} add_action('admin_menu', 'ee_add_menu_items');

wp_enqueue_script('wp-ajax-response');

function ee_render_about_page() {
	?>
		<div class="tool-box">
			<h3 class="title"><?php _e('Oferta') ?></h3>
			<p><?php _e('Acá me gustaría que yo pueda alimentarlo con la información de las chicas o si fuera posible incluso que ellas se puedan inscribir ahí mismo donde diga nombre, edad, lugar de residencia y el CV para abrir.');?></p>

			<h3 class="title"><?php _e('Demanda') ?></h3>
			<p><?php _e('Aquí la idea sería que los hoteles puedan con un formulario poner sus necesidades de contratación. Ojalá que también si fuera posible me llegara una notificación al correo para estar al tanto y dar seguimiento en cuanto un hotel suba un nuevo requerimiento.');?></p>

		</div>
	<?php
}

function ee_render_company_form() {
	?>
		<div class="wrap">
			<?php
				$action = $_REQUEST['action'];
				if( 'edit'===$action ) {
			?>
					<h2>Editar Hotel</h2>
			<?php
					global $wpdb;
					$company_id= $_REQUEST['company'];
					$data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_companies WHERE company_id = %s",$company_id), ARRAY_A);
					$company = $data[0];
					$company_name = $company['company_name'];
					$company_place = $company['company_place'];
					$company_position = $company['company_position'];
					$company_vacancy = $company['company_vacancy'];
					$company_contact = $company['company_contact'];
					$company_phone = $company['company_phone'];
					$company_email = $company['company_email'];
					$company_period = $company['company_period'];
					$company_comment = $company['company_comment'];
                    $company_logo = $company['company_logo'];

                    $imgOpen = "<img alt='' src='images/companies/";
                    $imgClose = "' style='width: 125px; height: 100px'/>";
                    $logo =  $imgOpen.$company_logo.$imgClose;

					echo sprintf('<form method="post" action="?page=%s&action=%s&company=%s&logo=%s" name="editcompany" id="editcompany" class="validate" novalidate="novalidate" enctype="multipart/form-data">','ee_companies','edit', $company_id, $company_logo);
			    } else {
			?>
					<h2>Añadir Hotel</h2>
			<?php
					echo sprintf('<form method="post" action="?page=%s&action=%s" name="createcompany" id="createcompany" class="validate" novalidate="novalidate" enctype="multipart/form-data">','ee_companies','add');
				}
			?>
				<table class="form-table">
					<tr class="form-field form-required">
						<th scope="row"><label for="company_name"><?php _e('Hotel'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
						<td><input name="company_name" type="text" id="company_name" value="<?php echo esc_attr($company_name); ?>" aria-required="true" /></td>
					</tr>
					<tr class="form-field form-required">
						<th scope="row"><label for="company_place"><?php _e('Puesto'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
						<td><input name="company_place" type="text" id="company_place" value="<?php echo esc_attr($company_place); ?>" aria-required="true" /></td>
					</tr>
					<tr class="form-field form-required">
						<th scope="row"><label for="company_position"><?php _e('Posición '); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
						<td><input name="company_position" type="text" id="company_position" value="<?php echo esc_attr($company_position); ?>" aria-required="true" /></td>
					</tr>
					<tr class="form-field form-required">
						<th scope="row"><label for="company_vacancy"><?php _e('Puestos Vacantes'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
						<td><input name="company_vacancy" type="text" id="company_vacancy" value="<?php echo esc_attr($company_vacancy); ?>" aria-required="true" /></td>
					</tr>
					<tr class="form-field form-required">
						<th scope="row"><label for="company_contact"><?php _e('Contacto'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
						<td><input name="company_contact" type="text" id="company_contact" value="<?php echo esc_attr($company_contact); ?>" aria-required="true" /></td>
					</tr>
					<tr class="form-field form-required">
						<th scope="row"><label for="company_phone"><?php _e('Telefono'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
						<td><input name="company_phone" type="text" id="company_phone" value="<?php echo esc_attr($company_phone); ?>" aria-required="true" /></td>
					</tr>
					<tr class="form-field form-required">
						<th scope="row"><label for="company_email"><?php _e('E-mail'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
						<td><input name="company_email" type="email" id="company_email" value="<?php echo esc_attr( $company_email ); ?>" /></td>
					</tr>
					<tr class="form-field">
						<th scope="row"><label for="company_period"><?php _e('Periodo') ?> </label></th>
						<td><input name="company_period" type="text" id="company_period" value="<?php echo esc_attr($company_period); ?>" /></td>
					</tr>
                    <div id="logo"></div>
                    <tr class="form-field">
                        <th scope="row"><label for="company_logo"><?php _e('Logo') ?> </label></th>
                        <td><input name="company_logo" type="file" id="company_logo" value="<?php echo esc_attr($company_logo); ?>" /></td>
                    </tr>
					<tr class="form-field">
						<th scope="row"><label for="company_comment"><?php _e('Comentarios') ?> </label></th>
						<td><textarea rows="4" cols="50" name="company_comment"  id="company_comment"><?php echo esc_attr($company_comment); ?></textarea></td>
					</tr>
				</table>
                <script language="javascript" type="text/javascript">
                    var $j = jQuery.noConflict();
                    $j(document).ready( function() {
                        $j('#logo').append("<?php echo $logo; ?>");
                    });
                </script>
			<?php
			/** This action is documented in wp-admin/company-new.php */
				//do_action( 'company_new_form', 'add-new-company' );
				if( 'edit'===$action ) {
					submit_button( __( 'Editar' ), 'primary', 'editcompany', true, array( 'id' => 'editcompanysub' ) );
				} else {
					submit_button( __( 'Añadir' ), 'primary', 'createcompany', true, array( 'id' => 'createcompanysub' ) );
				}
			?>
			</form>
		</div>
	<?php
}

/** *************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function ee_render_companies_page(){

	//Create an instance of our package class...
	$testListTable = new EE_Company_List_Table();
	//Fetch, prepare, sort, and filter our data...
	$testListTable->prepare_items();

	?>
	<div class="wrap">
		<div id="icon-users" class="icon32"><br/></div>
		<h2>Hoteles
			<?php echo sprintf('<a href="?page=%s" class="add-new-h2">Añadir nuevo</a>','ee_company_form') ?>
		</h2>
		<!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
		<form id="companies-filter" method="get">
			<!-- For plugins, we also need to ensure that the form posts back to our current page -->
			<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
			<!-- Now we can render the completed list table -->
			<?php $testListTable->display() ?>
		</form>
	</div>
	<div id="confirm-dialog"></div>
<?php
}

function ee_render_prospect_form() {
    ?>
    <div class="wrap">
        <?php
        $action = $_REQUEST['action'];
        if( 'edit'===$action ) {
            ?>
            <h2>Editar Prospecto</h2>
            <?php
            global $wpdb;
            $prospect_id= $_REQUEST['prospect'];
            $data = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_prospects WHERE prospect_id = %s",$prospect_id), ARRAY_A);
            $prospect = $data[0];
            $prospect_name = $prospect['prospect_name'];
            $prospect_phone = $prospect['prospect_phone'];
            $prospect_email = $prospect['prospect_email'];
            $prospect_comment = $prospect['prospect_comment'];
            $prospect_photo = $prospect['prospect_photo'];

            $imgOpen = "<img alt='' src='images/prospects/";
            $imgClose = "' style='width: 125px; height: 100px'/>";
            $photo =  $imgOpen.$prospect_photo.$imgClose;

            echo sprintf('<form method="post" action="?page=%s&action=%s&prospect=%s&photo=%s" name="editprospect" id="editprospect" class="validate" novalidate="novalidate" enctype="multipart/form-data">','ee_prospects','edit', $prospect_id, $prospect_photo);
        } else {
            ?>
            <h2>Añadir Prospecto</h2>
            <?php
            echo sprintf('<form method="post" action="?page=%s&action=%s" name="createprospect" id="createprospect" class="validate" novalidate="novalidate" enctype="multipart/form-data">','ee_prospects','add');
        }
        ?>
        <table class="form-table">
            <tr class="form-field form-required">
                <th scope="row"><label for="prospect_name"><?php _e('Nombre'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
                <td><input name="prospect_name" type="text" id="prospect_name" value="<?php echo esc_attr($prospect_name); ?>" aria-required="true" /></td>
            </tr>
            <tr class="form-field form-required">
                <th scope="row"><label for="prospect_phone"><?php _e('Telefono'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
                <td><input name="prospect_phone" type="text" id="prospect_phone" value="<?php echo esc_attr($prospect_phone); ?>" aria-required="true" /></td>
            </tr>
            <tr class="form-field form-required">
                <th scope="row"><label for="prospect_email"><?php _e('E-mail'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
                <td><input name="prospect_email" type="email" id="prospect_email" value="<?php echo esc_attr( $prospect_email ); ?>" /></td>
            </tr>
            <div id="photo"></div>
            <tr class="form-field">
                <th scope="row"><label for="prospect_photo"><?php _e('Foto') ?> </label></th>
                <td><input name="prospect_photo" type="file" id="prospect_photo" value="<?php echo esc_attr($prospect_photo); ?>" /></td>
            </tr>
            <tr class="form-field">
                <th scope="row"><label for="prospect_comment"><?php _e('Comentarios') ?> </label></th>
                <td><textarea rows="4" cols="50" name="prospect_comment"  id="prospect_comment"><?php echo esc_attr($prospect_comment); ?></textarea></td>
            </tr>
        </table>
        <script language="javascript" type="text/javascript">
            var $j = jQuery.noConflict();
            $j(document).ready( function() {
                $j('#photo').append("<?php echo $photo; ?>");
            });
        </script>
        <?php
        /** This action is documented in wp-admin/prospect-new.php */
        //do_action( 'prospect_new_form', 'add-new-prospect' );
        if( 'edit'===$action ) {
            submit_button( __( 'Editar' ), 'primary', 'editprospect', true, array( 'id' => 'editprospectsub' ) );
        } else {
            submit_button( __( 'Añadir' ), 'primary', 'createprospect', true, array( 'id' => 'createprospectsub' ) );
        }
        ?>
        </form>
    </div>
<?php
}

function ee_render_prospects_page(){

    //Create an instance of our package class...
    $prospectListTable = new EE_prospect_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $prospectListTable->prepare_items();

    ?>
    <div class="wrap">
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Prospectos
            <?php echo sprintf('<a href="?page=%s" class="add-new-h2">Añadir nuevo</a>','ee_prospect_form') ?>
        </h2>
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="companies-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $prospectListTable->display() ?>
        </form>
    </div>
    <div id="confirm-dialog"></div>
<?php
}