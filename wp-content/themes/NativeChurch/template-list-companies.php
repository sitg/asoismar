<?php
/**
 * Created by PhpStorm.
 * User: Jeffry Miranda
 * Date: 17/11/2014
 * Time: 01:19 PM
 */
/*
Template Name: Companies List
*/
get_header();  ?>
    <!-- start page section -->

    <link rel="stylesheet" type="text/css" href="/wp-content/themes/NativeChurch/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/1.0.2/css/dataTables.responsive.css"/>
    <script type='text/javascript' language='javascript' src="/wp-content/themes/NativeChurch/js/jquery.dataTables.min.js"></script>
    <script type='text/javascript' language='javascript' src="//cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js"></script>
    <section class="page-section">
        <div class="container">
            <div class="row">
                <!-- start post -->
                <article class="col-md-12">
                    <section class="page-content">
                        <?php
                        /*                        while(have_posts()):the_post();
                                                    the_content();
                                                endwhile;
                                                */?>
                        <table id="companyTable" class="display responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Hotel</th>
                                <th>Puesto</th>
                                <th>Posición</th>
                                <th>Vacantes</th>
                                <th>Contacto</th>
                                <th>Telefono</th>
                                <th>Email</th>
                                <th>Comentarios</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th>Hotel</th>
                                <th>Puesto</th>
                                <th>Posición</th>
                                <th>Vacantes</th>
                                <th>Contacto</th>
                                <th>Telefono</th>
                                <th>Email</th>
                                <th>Comentarios</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php
                            global $wpdb;
                            $companies = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_companies", null), ARRAY_A);
                            foreach ($companies as $company) : ?>
                                <tr>
                                    <td><img alt='' src='wp-admin/images/companies/<?php echo $company['company_logo'];?>'/></td>
                                    <td><?php echo $company['company_name'];?></td>
                                    <td><?php echo $company['company_place'];?></td>
                                    <td><?php echo $company['company_position'];?></td>
                                    <td><?php echo $company['company_vacancy'];?></td>
                                    <td><?php echo $company['company_contact'];?></td>
                                    <td><?php echo $company['company_phone'];?></td>
                                    <td><?php echo $company['company_email'];?></td>
                                    <td><?php echo $company['company_comment'];?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </section>
                    <?php if ($imic_options['switch_sharing'] == 1 && $imic_options['share_post_types']['2'] == '1') { ?>
                        <?php imic_share_buttons(); ?>
                    <?php } ?>
                </article>
                <!-- end post -->
            </div>
        </div>
    </section>
    <script>
        var $ = jQuery.noConflict();
        $(document).ready(function(){
            $('#companyTable').DataTable({
                responsive: true,
                "order": [[ 0, "asc" ]],
                "oLanguage": {
                    "sLengthMenu": "Mostrando _MENU_ entradas",
                    "sZeroRecords": "Disculpe, datos no encntrados.",
                    "sInfo": "Mostrando _PAGE_ de _PAGES_",
                    "sInfoEmpty": "No hay registros disponibles",
                    "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                    "sSearch": "Filtrar: ",
                    "oPaginate": {
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
    </script>
<?php get_footer(); ?>