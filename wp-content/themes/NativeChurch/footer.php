<?php
global $imic_options;
$show_on_front = get_option('show_on_front');
 if ((!is_front_page()) || $show_on_front == 'posts'||(!is_page_template('template-home.php')&&!is_page_template('template-h-second.php')&&!is_page_template('template-h-third.php'))) {
     echo '</div></div>';
}
?>
<?php $options = get_option('imic_options'); ?>
<!-- Start Footer -->
<?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
<footer class="site-footer">
    <div class="container">
        <div class="row">
        	<?php dynamic_sidebar('footer-sidebar'); ?>
        </div>
    </div>
</footer>
<?php endif; ?>
<footer class="site-footer-bottom">
    <div class="container">
        <div class="row">
            <?php
            if (!empty($options['footer_copyright_text'])) {
                echo '<div class="copyrights-col-left col-md-6 col-sm-6">'; ?>
                <p><?php _e('&copy; ','framework'); echo date('Y '); bloginfo('name'); _e('. ','framework'); echo $options['footer_copyright_text']; ?></p>
                <?php echo '</div>';
            }
            ?>
            <div class="copyrights-col-right col-md-6 col-sm-6">
                <div class="social-icons">
                    <?php
                    $socialSites = $imic_options['footer_social_links'];
			foreach($socialSites as $key => $value) {
				if(filter_var($value, FILTER_VALIDATE_URL)){ 
					echo '<a href="'. $value .'" target="_blank"><i class="fa '. $key .'"></i></a>';
				}
			}
                    ?>
                  </div>
            </div>
        </div>
    </div>
</footer>
<?php if ($options['enable_backtotop'] == 1) { 
echo'<a id="back-to-top"><i class="fa fa-angle-double-up"></i></a>';
 } ?>
</div>
<!-- End Boxed Body -->
<?php wp_footer(); ?>
</body>
</html>