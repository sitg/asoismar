<?php
/**
 * Created by PhpStorm.
 * User: Jeffry Miranda
 * Date: 17/11/2014
 * Time: 11:05 AM
 */
/*
Template Name: Prospect List
*/
get_header();  ?>
    <!-- start page section -->

    <link rel="stylesheet" type="text/css" href="/wp-content/themes/NativeChurch/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/1.0.2/css/dataTables.responsive.css"/>
    <script type='text/javascript' language='javascript' src="/wp-content/themes/NativeChurch/js/jquery.dataTables.min.js"></script>
    <script type='text/javascript' language='javascript' src="//cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js"></script>
    <section class="page-section">
        <div class="container">
            <div class="row">
                <!-- start post -->
                <article class="col-md-12">
                    <section class="page-content">
                        <?php
/*                        while(have_posts()):the_post();
                            the_content();
                        endwhile;
                        */?>
                        <table id="prospectTable" class="display" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Comentarios</th>
                                <!--<th>Sigame en </th>-->
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Comentarios</th>
                                <!--<th>Sigame en</th>-->
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php
                            global $wpdb;
                            $prospects = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_prospects", null), ARRAY_A);
                            foreach ($prospects as $prospect) : ?>
                                <tr>
                                    <td><?php echo $prospect['prospect_name'];?></td>
                                    <td><?php echo $prospect['prospect_email'];?></td>
                                    <td><?php echo $prospect['prospect_comment'];?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </section>
                    <?php if ($imic_options['switch_sharing'] == 1 && $imic_options['share_post_types']['2'] == '1') { ?>
                        <?php imic_share_buttons(); ?>
                    <?php } ?>
                </article>
                <!-- end post -->
            </div>
        </div>
    </section>
    <script>
        var $ = jQuery.noConflict();
        $(document).ready(function(){
            $('#prospectTable').DataTable({
                responsive: true,
                "order": [[ 0, "asc" ]],
                "oLanguage": {
                    "sLengthMenu": "Mostrando _MENU_ entradas",
                    "sZeroRecords": "Disculpe, datos no encntrados.",
                    "sInfo": "Mostrando _PAGE_ de _PAGES_",
                    "sInfoEmpty": "No hay registros disponibles",
                    "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                    "sSearch": "Filtrar: ",
                    "oPaginate": {
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
        });
    </script>
<?php get_footer(); ?>