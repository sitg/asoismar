<?php
/*
Template Name: Events Grid
*/ 
get_header();
$pageOptions = imic_page_design(); //page design options 
echo '<div class="container">
<div class="row">'; ?>
<div class="<?php echo $pageOptions['class']; ?>">
  <?php 
  
  while(have_posts()):the_post();
  if($post->post_content!="") :
                              the_content();        
                              echo '<div class="spacer-20"></div>';
                      endif;	
  endwhile; ?> 
<?php
$event_add = array();
$rec = 1;
$no_event = 0;
$today = date('Y-m-d');
$event_category = get_post_meta(get_the_ID(),'imic_advanced_event_list_taxonomy',true);
						if(!empty($event_category)){
						$event_categories= get_term_by('id',$event_category,'event-category');
						$event_category= $event_categories->slug; }
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts(array('post_type' => 'event','event-category' => $event_category, 'meta_key' => 'imic_event_start_dt','meta_query' => array( array( 'key' => 'imic_event_frequency_end', 'value' => $today, 'compare' => '>=') ), 'orderby' => 'meta_value', 'order' => 'ASC', 'posts_per_page'=>50));
if(have_posts()):while(have_posts()):the_post();
$frequency = get_post_meta(get_the_ID(), 'imic_event_frequency', true);
$frequency_count = 0;
$frequency_count = get_post_meta(get_the_ID(), 'imic_event_frequency_count', true);
if ($frequency_count > 0) {
$frequency_count = $frequency_count;
}
else {
$frequency_count = 0;
}
$date_diff = $frequency * 86400;
$sinc = 0;
while ($sinc <= $frequency_count) {
$diff_date = $sinc * $date_diff;
$st_date = get_post_meta(get_the_ID(), 'imic_event_start_dt', true);
$eventTime = get_post_meta(get_the_ID(), 'imic_event_start_tm', true);
$eventTime = ($eventTime!='')?$eventTime:'23:59';
if($frequency==30) {
$st_date = strtotime($st_date.' '.$eventTime);
$diff_date = strtotime("+".$sinc." month", $st_date);
}
else {
$st_date = strtotime($st_date.' '.$eventTime);
$diff_date = $st_date + $diff_date;
}
if($diff_date>=date('U')&&  has_post_thumbnail()) {
$event_add[$diff_date + $rec] = get_the_ID();
$no_event++;
}
$sinc++; $rec++; }
endwhile; endif;
wp_reset_query();
if($no_event==0):
echo '<article class="post">';
if (current_user_can('edit_posts')) :
echo '<h3>'.__('There are no future events to show.', 'framework').'</h3>';
echo '</article>';
endif;
endif; // end have_posts()
$now = date('U');
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$count = 1;
$grid_item = 1;
$perPage = get_option('posts_per_page');
$paginate = 1;
if($paged>1) {
$paginate = ($paged-1)*$perPage; $paginate = $paginate+1; }
$TotalEvents = count($event_add);
if($TotalEvents%$perPage==0) {
$TotalPages = $TotalEvents/$perPage;
}
else {
$TotalPages = $TotalEvents/$perPage;
$TotalPages = $TotalPages+1;
}
ksort($event_add);
echo '<ul class="grid-holder col-3 events-grid">';
foreach ($event_add as $key => $value) {
setup_postdata(get_post($value));
$eventStartDate = strtotime(get_post_meta($value,'imic_event_start_dt',true));
$eventStartTime = strtotime(get_post_meta($value,'imic_event_start_tm',true));
$eventEndTime = strtotime(get_post_meta($value,'imic_event_end_tm',true));
$registration_status = get_post_meta($value,'imic_event_registration_status',true);
/** Event Details Manage **/
if($registration_status==1&&(function_exists('imic_get_currency_symbol'))) {
$eventDetailIcons = array('fa-calendar', 'fa-map-marker','fa-money');	
}else {
$eventDetailIcons = array('fa-calendar', 'fa-map-marker'); }
$stime = ""; $etime = "";
if($eventStartTime!='') { $stime = ' | ' .date_i18n(get_option('time_format'), $eventStartTime) ; }
if($eventEndTime!='') { $etime =  ' - '. date_i18n(get_option('time_format'),$eventEndTime); }
if($registration_status==1&&(function_exists('imic_get_currency_symbol'))) {
$eventDetailsData = array(date_i18n('j M, ',$key).date_i18n('l',$key). $stime .  $etime, get_post_meta($value,'imic_event_address',true),imic_get_currency_symbol(get_option('paypal_currency_options')).get_post_meta($value,'imic_event_registration_fee',true));	
}else {
$eventDetailsData = array(date_i18n('j M, ',$key).date_i18n('l',$key). $stime .  $etime, get_post_meta($value,'imic_event_address',true)); }
$eventValues = array_filter($eventDetailsData, 'strlen');
if($count==$paginate&&$grid_item<=$perPage) { $paginate++; $grid_item++;
$frequency = get_post_meta($value,'imic_event_frequency',true); 
//if ('' != get_the_post_thumbnail($value)) {
echo '<li class="grid-item format-standard">';
$date_converted=date('Y-m-d',$key );
$custom_event_url =imic_query_arg($date_converted,$value); 
echo '<div class="grid-item-inner">';
echo '<a href="'.$custom_event_url.'" class="media-box">';
echo get_the_post_thumbnail($value, 'full');
echo '</a>';
echo '<div class="grid-content">';
echo '<h3><a href="' . $custom_event_url. '">' . get_the_title($value).'</a>'.imicRecurrenceIcon($value).'</h3>';
echo imic_excerpt(25);
echo'</div>';
if(!empty($eventValues)){ 
echo '<ul class="info-table">';
$flag = 0;
foreach($eventDetailsData as $edata){
if(!empty($edata)){
echo '<li><i class="fa '.$eventDetailIcons[$flag].'"></i> '.$edata.' </li>';
}				
$flag++;	
}
echo '</ul>';
//}
echo '</div>
</li>';
 }} $count++; }
echo '</ul>';
wp_reset_postdata();
pagination($TotalPages,$perPage);
echo '</div>';
?>
            <?php if(!empty($pageOptions['sidebar'])){ ?>
            <!-- Start Sidebar -->
            <div class="col-md-3 sidebar">
                <?php dynamic_sidebar($pageOptions['sidebar']); ?>
            </div>
            <!-- End Sidebar -->
            <?php } ?>
    	</div>
     </div>
<?php get_footer(); ?>